var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Projekt = require('../models/Projekt.js');

var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
  service: 'Gmail',
  tls: {
    rejectUnauthorized: false
  },
  auth: {
    user: 'tehnicendolg.pkp@gmail.com',
    pass: 'PKPTehnicenDolg123!'
  }
});

const checkString = "ZdaDrXkzstRLn99P6bVhxTeBv2tY3T";

  
/* GET ALL PROJEKTI */

router.get('/', function(req, res, next) {
  var auth = req.get('Authorization');
  if (auth != checkString) {
    err.status = 401;
    res.json(false);
  }

  Projekt.find(function (err, products) {
    if (err) return next(err);
    res.json(products);
  });
});

/* GET SINGLE PROJEKT BY ID */
router.get('/:id', function(req, res, next) {
  var auth = req.get('Authorization');
  if (auth != checkString) {
    err.status = 401;
    res.json(false);
  }

  Projekt.findById(req.params.id, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* GET ALL PROJEKTI BY ID */
router.get('/email/:email', function(req, res, next) {
  var auth = req.get('Authorization');
  if (auth != checkString) {
    err.status = 401;
    res.json(false);
  }

  var email = req.params.email;
  Projekt.find({ $or:[ {'clan':email},  {'clan1':email}, {'clan2':email}, {'clan3':email}, {'clan4':email}, {'clan5':email}, {'clan6':email}, {'clan7':email}, {'clan8':email}, {'clan9':email}, ]},function (err, products) {
    if (err) return next(err);
    res.json(products);
  });
});

/* SAVE PROJEKT */
router.post('/', function(req, res, next) {
  var auth = req.get('Authorization');
  if (auth != checkString) {
    err.status = 401;
    res.json(false);
  }

  Projekt.create(req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);

    const mailOptions = {
      from: 'tehnicendolg.pkp@gmail.com', // sender address
      to: post['clan']+', '+post['clan1']+','+post['clan2']+','+post['clan3']+','+post['clan4']+','+post['clan5']+','+post['clan6']+','+post['clan7']+','+post['clan8']+','+post['clan9'], // list of receivers
      subject: 'Platforma tehničen dolg', // Subject line
      html: '<p>Pozdravljeni!<br><br>Dodani ste bili v projekt '+post['naziv']+' na spletni platformi za upravljanje tehničnega dolga.' +
      '<br>Registrirajte se z elektronskim naslovom na katerega ste prejeli to sporočilo.</p>'// plain text body
    };

    transporter.sendMail(mailOptions, function (err, info) {
      if(err)
        console.log(err)
      else
        console.log(info);
    });

  });
});

/* UPDATE PROJEKT */
router.put('/:id', function(req, res, next) {
  var auth = req.get('Authorization');
  if (auth != checkString) {
    err.status = 401;
    res.json(false);
  }

  Projekt.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* DELETE PROJEKT */
router.delete('/:id', function(req, res, next) {
  var auth = req.get('Authorization');
  if (auth != checkString) {
    err.status = 401;
    res.json(false);
  }
  
  Projekt.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

module.exports = router;
