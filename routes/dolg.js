var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var DOlg = require('../models/Dolg.js');

const checkString = "ZdaDrXkzstRLn99P6bVhxTeBv2tY3T";

/* GET ALL DOLGOVI */
router.get('/', function(req, res, next) {
  var auth = req.get('Authorization');
  if (auth != checkString) {
    err.status = 401;
    res.json(false);
  }

  DOlg.find(function (err, products) {
    if (err) return next(err);
    res.json(products);
  });
});

/* GET ALL DOLGOVI BY EMAIL */
router.get('/email/:email', function(req, res, next) {
  var auth = req.get('Authorization');
  if (auth != checkString) {
    err.status = 401;
    res.json(false);
  }

  var email = req.params.email;
  DOlg.find({ $or:[ {'clan':email},  {'clan1':email}, {'clan2':email}, {'clan3':email}, {'clan4':email}, {'clan5':email}, {'clan6':email}, {'clan7':email}, {'clan8':email}, {'clan9':email}, ]},function (err, products) {
    if (err) return next(err);
    res.json(products);
  });
});

/* GET SINGLE DOLG BY ID */
router.get('/:id', function(req, res, next) {
  var auth = req.get('Authorization');
  if (auth != checkString) {
    err.status = 401;
    res.json(false);
  }

  DOlg.findById(req.params.id, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* SAVE DOLG */
router.post('/', function(req, res, next) {
  var auth = req.get('Authorization');
  if (auth != checkString) {
    err.status = 401;
    res.json(false);
  }

  DOlg.create(req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* UPDATE DOLG */
router.put('/:id', function(req, res, next) {
  var auth = req.get('Authorization');
  if (auth != checkString) {
    err.status = 401;
    res.json(false);
  }

  DOlg.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* DELETE DOLG */
router.delete('/:id', function(req, res, next) {
  var auth = req.get('Authorization');
  if (auth != checkString) {
    err.status = 401;
    res.json(false);
  }
  
  DOlg.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

module.exports = router;
