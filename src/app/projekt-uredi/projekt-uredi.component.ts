import {Component, Inject, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {ApiService} from '../api.service';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {UrediDolgDialogOverviewDialogComponent} from '../dolg-uredi/dolg-uredi.component';

@Component({
  selector: 'app-projekt-uredi',
  templateUrl: './projekt-uredi.component.html',
  styleUrls: ['./projekt-uredi.component.css']
})
export class ProjektUrediComponent {

  constructor(public dialog: MatDialog) {
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(UrediProjektDialogOverviewDialogComponent, {
      width: '750px',
      height: '600px'
    });

    dialogRef.afterClosed().subscribe(result => {});
  }


}

@Component({
  selector: 'app-projekt-uredi-dialog',
  templateUrl: './projekt-uredi-dialog.component.html',
  styleUrls: ['./projekt-uredi.component.css']
})
export class UrediProjektDialogOverviewDialogComponent implements OnInit {

  projektForm: FormGroup;
  naziv = '';
  zgornja_meja_dolga = '';
  tip_ocene: any;

  id = '';

  constructor(private router: Router, private api: ApiService, private formBuilder: FormBuilder,
              public dialogRef: MatDialogRef<UrediProjektDialogOverviewDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.getProjekt(this.router.url.slice(21, 45));
    this.projektForm = this.formBuilder.group({
      'naziv' : [null, Validators.required],
      'zgornja_meja_dolga' : [null, Validators.required]
    });
  }

  getProjekt(id) {
    this.api.getProjekt(id).subscribe(data => {
      this.id = data._id;
      this.tip_ocene = data.tip_ocena;
      this.projektForm.setValue({
        naziv : data.naziv,
        zgornja_meja_dolga : data.zgornja_meja_dolga
      });
    });
  }

  onFormSubmit(form: NgForm) {
    this.api.updateProjekt(this.id, form)
      .subscribe(res => {
          const id = res['_id'];
        this.dialogRef.close();
        this.router.navigateByUrl('/projekti', {skipLocationChange: true}).then(() =>
          this.router.navigate(['/', { outlets: {primary: ['projekt-podrobnosti',id], dolg: ['dolgovi-od-projekta'] } }]));
        }, (err) => {
          console.log(err);
        }
      );
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
