import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DolgoviOdProjektaComponent } from './dolgovi-od-projekta.component';

describe('DolgoviOdProjektaComponent', () => {
  let component: DolgoviOdProjektaComponent;
  let fixture: ComponentFixture<DolgoviOdProjektaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DolgoviOdProjektaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DolgoviOdProjektaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
