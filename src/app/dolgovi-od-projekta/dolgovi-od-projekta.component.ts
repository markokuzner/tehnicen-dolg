import {Component, Inject, Input, OnDestroy, OnInit} from '@angular/core';
import {ApiService} from '../api.service';
import {Router} from '@angular/router';
import {MAT_DIALOG_DATA, MatBottomSheet, MatBottomSheetRef, MatDialog, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-dolgovi-od-projekta',
  templateUrl: './dolgovi-od-projekta.component.html',
  styleUrls: ['./dolgovi-od-projekta.component.css']
})
export class DolgoviOdProjektaComponent implements OnInit, OnDestroy {

  loadCompleted = false;
  tiles = [];
  podatki = [];
  lineChartData = {
    chartType: 'LineChart',
    dataTable: [],
    options: {
      hAxis: {
        title: 'Čas'
      },
      vAxis: {
        title: 'Količina dolga'
      },
      colors: ['#424242', '#b71c1c'],
      width: 975,
      height: 180,
      series: {
        0: {},
        1: {
          lineWidth: 5,
          lineDashStyle: [4, 4]
        }
      }
    },
  };
  tip_ocene: string;

  constructor(private api: ApiService, private router: Router, private bottomSheet: MatBottomSheet) {}

  ngOnInit() {
    this.api.getDolgovi()
      .subscribe(res => {
          this.api.getProjekt(this.router.url.slice(21, 45))
            .subscribe(data => {
              const glava = ['število', 'Količina dolga', 'Meja dovoljenega dolga'];
              this.podatki.push(glava);
              const meja = +data.zgornja_meja_dolga;
              const prva_vrstica = [new Date(data.datum).toLocaleDateString(), 0, meja];
              this.podatki.push(prva_vrstica);
              let sestevek = 0;
              let stevec = 2;
              for (const i in res) {
                if (res[i].id_projekt === this.router.url.slice(21, 45)) {
                  const dolg = {naziv: res[i].naziv,
                    id_dolga: res[i]._id,
                    vzrok: res[i].vzrok,
                    tip: res[i].tip,
                    podtip : res[i].podtip,
                    opis: res[i].opis,
                    obseg: res[i].obseg,
                    ocena: res[i].ocena,
                    kriticnost: res[i].kriticnost,
                    status: res[i].status,
                    datum: res[i].datum,
                    cols: 1, rows: 1, color: 'white'};
                  this.tiles.push(dolg);
                  const stevilo = +res[i].ocena;
                  const polje = [new Date(res[i].datum).toLocaleDateString(), stevilo + sestevek, meja];
                  this.podatki.push(polje);
                  sestevek = sestevek + stevilo;
                  stevec++;
                }
              }
              if ( sestevek > meja ) {
                const update_projekt = {
                  'error' : 'true',
                };
                this.api.updateProjekt(this.router.url.slice(21, 45), JSON.stringify(update_projekt))
                  .subscribe(res1 => {
                      const id = res1['_id'];
                    }, (err) => {
                      console.log(err);
                    }
                  );
                this.bottomSheet.open(BottomSheetOverviewSheetComponent);
              } else {
                const update_projekt = {
                  'error' : 'false',
                };
                this.api.updateProjekt(this.router.url.slice(21, 45), JSON.stringify(update_projekt))
                  .subscribe(res1 => {
                      const id = res1['_id'];
                    }, (err) => {
                      console.log(err);
                    }
                  );
              }
              this.tip_ocene = data.tip_ocena;
            });
          this.lineChartData.dataTable = this.podatki;
        }, err => {
          console.log(err);
        });
    setTimeout(() => this.loadCompleted = true, 500);
  }


  ngOnDestroy() {
    this.loadCompleted = false;
  }
}

@Component({
  selector: 'app-bottom-sheet-overview-sheet',
  templateUrl: 'bottom-sheet-overview-sheet.html',
  styleUrls: ['./dolgovi-od-projekta.component.css']
})
export class BottomSheetOverviewSheetComponent {
  constructor(private bottomSheetRef: MatBottomSheetRef<BottomSheetOverviewSheetComponent>) {}
}

