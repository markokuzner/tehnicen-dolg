import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import {HttpClient, HttpHeaders, HttpErrorResponse, HttpParams} from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json', 'Authorization': 'ZdaDrXkzstRLn99P6bVhxTeBv2tY3T'})
};
const apiUrlProjekt = '/apiProjekt';
const apiUrlDolg = '/apiDolg';

@Injectable({ providedIn: 'root' })
export class ApiService {

  constructor(private http: HttpClient) { }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // napaka na strani userja ali mreže
      console.error('Napaka:', error.error.message);
    } else {
      // backend vrne neuspešno kodo
      // namig kaj je narobe
      console.error(
        `Backend vrnjena koda ${error.status}, ` +
        `namig: ${error.error}`);
    }
    // vrne observable napako
    return throwError('Napaka, poskusi ponovno kasneje.'); }

  private extractData(res: Response) {
    const body = res;
    return body || { };
  }

  getProjekti(): Observable<any> {
    return this.http.get(apiUrlProjekt, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  getProjekt(id: string): Observable<any> {
    const url = `${apiUrlProjekt}/${id}`;
    return this.http.get(url, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  postProjekt(data): Observable<any> {
    return this.http.post(apiUrlProjekt, data, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  updateProjekt(id: string, data): Observable<any> {
    const url = `${apiUrlProjekt}/${id}`;
    return this.http.put(url, data, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  deleteProjekt(id: string): Observable<{}> {
    const url = `${apiUrlProjekt}/${id}`;
    return this.http.delete(url, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  getDolgovi(): Observable<any> {
    return this.http.get(apiUrlDolg, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  getDolg(id: string): Observable<any> {
    const url = `${apiUrlDolg}/${id}`;
    return this.http.get(url, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  postDolg(data): Observable<any> {
    return this.http.post(apiUrlDolg, data, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  updateDolg(id: string, data): Observable<any> {
    const url = `${apiUrlDolg}/${id}`;
    return this.http.put(url, data, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  deleteDolg(id: string): Observable<{}> {
    const url = `${apiUrlDolg}/${id}`;
    return this.http.delete(url, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

}
