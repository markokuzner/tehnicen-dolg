import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ProjektComponent } from './projekt/projekt.component';
import { ProjektPodrobnostiComponent, IzbrisOnemogocenComponent } from './projekt-podrobnosti/projekt-podrobnosti.component';
import { ProjektUstvariComponent,  UstavriProjektDialogOverviewDialogComponent} from './projekt-ustvari/projekt-ustvari.component';
import { ProjektUrediComponent, UrediProjektDialogOverviewDialogComponent } from './projekt-uredi/projekt-uredi.component';

import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatTableModule,
  MatIconModule,
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule,
  MatSnackBarModule,
  MatSidenavModule,
  MatDialogModule,
  MatTooltipModule,
  MatSelectModule,
  MatTabsModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatChipsModule,
  MatListModule,
  MatDividerModule,
  MatSlideToggleModule,
  MatExpansionModule

} from '@angular/material';

import {MatMenuModule} from '@angular/material/menu';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatGridListModule} from '@angular/material/grid-list';
import { DolgComponent } from './dolg/dolg.component';
import { DolgPodrobnostiComponent } from './dolg-podrobnosti/dolg-podrobnosti.component';
import { DolgUrediComponent, UrediDolgDialogOverviewDialogComponent } from './dolg-uredi/dolg-uredi.component';
import { DolgUstvariComponent, UstavriDolgDialogOverviewDialogComponent } from './dolg-ustvari/dolg-ustvari.component';
import { DolgoviOdProjektaComponent, BottomSheetOverviewSheetComponent } from './dolgovi-od-projekta/dolgovi-od-projekta.component';
import { DomovComponent } from './domov/domov.component';

import { AuthService } from './auth/auth.service';
import { AuthGuard } from './auth/auth.guard';
import {DialogOverviewDialogComponent, ProfilComponent} from './profil/profil.component';
import { SlikaProfilComponent } from './slika-profil/slika-profil.component';

import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { SpinnerComponent } from './spinner/spinner.component';

const appRoutes: Routes = [
  {
    path: '',
    component: DomovComponent,
    data: { title: 'Domov' },
    outlet: 'domov'
  },
  {
    path: 'projekti',
    component: ProjektComponent,
    data: { title: 'Seznam projektov' },
    canActivate: [
      AuthGuard
    ]
  },
  {
    path: 'projekt-podrobnosti/:id',
    component: ProjektPodrobnostiComponent,
    data: { title: 'Podrobnosti projekta' },
    canActivate: [
      AuthGuard
    ]
  },
  {
    path: 'projekt-uredi/:id',
    component: ProjektUrediComponent,
    data: { title: 'Uredi projekt' },
    canActivate: [
      AuthGuard
    ]
  },
  {
    path: 'dolgovi',
    component: DolgComponent,
    data: { title: 'Seznam dolgov'},
    outlet: 'dolg',
    canActivate: [
      AuthGuard
    ]
  },
  {
    path: 'dolg-podrobnosti/:id',
    component: DolgPodrobnostiComponent,
    data: { title: 'Podrobnosti dolga'},
    outlet: 'dolg',
    canActivate: [
      AuthGuard
    ]
  },
  {
    path: 'dolg-uredi/:id',
    component: DolgUrediComponent,
    data: { title: 'Uredi dolg'},
    outlet: 'dolg',
    canActivate: [
      AuthGuard
    ]
  },
  {
    path: 'dolgovi-od-projekta',
    component: DolgoviOdProjektaComponent,
    data: { title: 'Dolgovi od projekta'},
    outlet: 'dolg',
    canActivate: [
      AuthGuard
    ]
  },
  {
    path: 'nalaganje',
    component: SpinnerComponent,
    data: { title: 'Nalaganje'},
    outlet: 'dolg',
    canActivate: [
      AuthGuard
    ]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    ProjektComponent,
    ProjektPodrobnostiComponent,
    IzbrisOnemogocenComponent,
    ProjektUstvariComponent,
    ProjektUrediComponent,
    DolgComponent,
    DolgPodrobnostiComponent,
    DolgUrediComponent,
    DolgUstvariComponent,
    DolgoviOdProjektaComponent,
    DomovComponent,
    ProfilComponent,
    DialogOverviewDialogComponent,
    SlikaProfilComponent,
    UstavriProjektDialogOverviewDialogComponent,
    UstavriDolgDialogOverviewDialogComponent,
    BottomSheetOverviewSheetComponent,
    SpinnerComponent,
    UrediDolgDialogOverviewDialogComponent,
    UrediProjektDialogOverviewDialogComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatMenuModule,
    MatToolbarModule,
    MatGridListModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatDialogModule,
    MatTooltipModule,
    MatSelectModule,
    MatTabsModule,
    Ng2GoogleChartsModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatChipsModule,
    MatListModule,
    MatDividerModule,
    MatSlideToggleModule,
    MatExpansionModule
  ],
  providers: [AuthService, AuthGuard],
  bootstrap: [AppComponent],
  entryComponents: [ProjektPodrobnostiComponent,
    IzbrisOnemogocenComponent,
    DialogOverviewDialogComponent,
    UstavriProjektDialogOverviewDialogComponent,
    UstavriDolgDialogOverviewDialogComponent,
    BottomSheetOverviewSheetComponent,
    UrediDolgDialogOverviewDialogComponent,
    UrediProjektDialogOverviewDialogComponent,
  ],
})
export class AppModule { }
