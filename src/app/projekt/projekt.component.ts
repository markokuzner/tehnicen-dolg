import {Component, OnInit, ViewChild} from '@angular/core';
import { ApiService } from '../api.service';
import {MatSort, MatTableDataSource} from '@angular/material';
import {AuthService} from '../auth/auth.service';


@Component({
  selector: 'app-projekt',
  templateUrl: './projekt.component.html',
  styleUrls: ['./projekt.component.css']
})
export class ProjektComponent implements OnInit {

  loadCompleted = false;
  displayedColumns = ['naziv', 'datum', 'alert'];
  dataSource: any;
  profile: any;

  constructor(private api: ApiService, public auth: AuthService) { }

  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.api.getProjekti()
      .subscribe(res => {
        if (this.auth.userProfile) {
          this.profile = this.auth.userProfile;
          console.log("stored profile email: " + this.profile.email);
          this.processData(res);
        } else {
          this.auth.getProfile((err, profile) => {
            this.profile = profile;
            console.log("profile email: " + this.profile.email);
            this.processData(res);
          });
        }
        }, err => {
        console.log(err);
        this.getProjekts();
      });
   //this.getProjekts();
  }

  ngOnDestroy() {
    this.loadCompleted = false;
  }

  getProjekts(): void {
    this.api.getProjekti()
      .subscribe(res => {
          this.getProfile(res);
        }, err => {
        console.log(err);
        this.getProjekts();
      });
  }

  processData(res: any): void {
    let podatki = [];
            for (let i in res) {
              podatki = podatki.concat([res[i]]);
            }
            let email: string;
            this.dataSource = new MatTableDataSource(podatki);
            email = this.profile.email.trim();
            email = email.toLowerCase();
            this.dataSource.filter = email;
            this.dataSource.sort = this.sort;
            this.loadCompleted = true;
  }

  getProfile(res: any): void {
    this.auth.getProfile((err, profile) => {
      if(err){
        console.log("myerror: " + err);
        this.getProfile(res);
      }else{
        let podatki = [];
        for (let i in res) {
          podatki = podatki.concat([res[i]]);
        }
        let email: string;
        this.dataSource = new MatTableDataSource(podatki);
        email = profile.email.trim();
        email = email.toLowerCase();
        this.dataSource.filter = email;
        this.dataSource.sort = this.sort;
      }
    });
  }
}
