import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DolgUstvariComponent } from './dolg-ustvari.component';

describe('DolgUstvariComponent', () => {
  let component: DolgUstvariComponent;
  let fixture: ComponentFixture<DolgUstvariComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DolgUstvariComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DolgUstvariComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
