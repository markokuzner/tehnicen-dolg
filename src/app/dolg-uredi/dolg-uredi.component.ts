import {Component, Inject, OnInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import {Location} from '@angular/common';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-dolg-uredi',
  templateUrl: './dolg-uredi.component.html',
  styleUrls: ['./dolg-uredi.component.css']
})
export class DolgUrediComponent implements OnInit {

  id = '';

  constructor(public dialog: MatDialog, private _location: Location, private route: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(UrediDolgDialogOverviewDialogComponent, {
      width: '750px',
      height: '700px',
      data: {id: this.id}
    });

    dialogRef.afterClosed().subscribe(result => {});
  }

}


@Component({
  selector: 'app-dolg-uredi-dialog',
  templateUrl: './dolg-uredi-dialog.component.html',
  styleUrls: ['./dolg-uredi.component.css']
})
export class UrediDolgDialogOverviewDialogComponent implements OnInit {

  dolgForm: FormGroup;
  naziv = '';
  vzrok = '';
  tip = '';
  podtip = '';
  opis = '';
  obseg = '';
  ocena = '';
  kriticnost = '';
  status = '';
  id_projekt = '';
  projekt = {};

  projekt_ocena = '';

  tip1 = false;
  tip2 = false;
  tip3 = false;
  tip4 = false;
  tip5 = false;
  tip6 = false;
  tip7 = false;
  tip8 = false;
  tip9 = false;
  tip10 = false;
  tip11 = false;
  tip12 = false;
  tip13 = false;
  tip14 = false;
  tip15 = false;

  id = '';

  boolean_tip = '';
  boolean_vzrok = '';

  constructor(private router: Router, private api: ApiService, private formBuilder: FormBuilder,
              public dialogRef: MatDialogRef<UrediDolgDialogOverviewDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.getDolg(this.data.id);
    this.dolgForm = this.formBuilder.group({
      'naziv' : [null, Validators.required],
      'vzrok' : [null],
      'tip' : [null],
      'podtip' : [null],
      'opis' : [null, Validators.required],
      'obseg' : [null, Validators.required],
      'ocena' : [null, Validators.required],
      'kriticnost' : [null, Validators.required],
      'status' : [null, Validators.required]
    });
  }

  getDolg(id) {
    this.api.getDolg(id).subscribe(data => {
      this.id = data._id;
      this.boolean_tip = data.tip;
      this.boolean_vzrok = data.vzrok;
      this.id_projekt = data.id_projekt;
      this.getProjektPodrobnosti(data.id_projekt);
      this.dolgForm.setValue({
        naziv : data.naziv,
        vzrok : data.vzrok,
        tip : data.tip,
        podtip : data.podtip,
        opis : data.opis,
        obseg : data.obseg,
        ocena : data.ocena,
        kriticnost : data.kriticnost,
        status : data.status,
      });
    });
  }

  onFormSubmit(form: NgForm) {
    this.api.updateDolg(this.id, form)
      .subscribe(res => {
          const id = res['_id'];
          this.dialogRef.close();
          if (this.router.url.includes('projekti')) {
            this.router.navigateByUrl('/projekti', {skipLocationChange: true}).then(() =>
              this.router.navigate(['/', { outlets: { dolg: ['dolg-podrobnosti', this.id] } }]));
          } else {
          this.router.navigateByUrl('/projekt-podrobnosti/' + this.router.url.slice(21, 45), {skipLocationChange: true}).then(() =>
          this.router.navigate(['/', { outlets: { dolg: ['dolg-podrobnosti', this.id] } }]));
          }
        }, (err) => {
          console.log(err);
        }
      );
  }

  getProjektPodrobnosti(id) {
    this.api.getProjekt(id)
      .subscribe(data => {
        this.projekt = data;
        this.projekt_ocena = data.tip_ocena;
      });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  tip_1(): void {
    this.tip1 = true;
    this.tip2 = false;
    this.tip3 = false;
    this.tip4 = false;
    this.tip5 = false;
    this.tip6 = false;
    this.tip7 = false;
    this.tip8 = false;
    this.tip9 = false;
    this.tip10 = false;
    this.tip11 = false;
    this.tip12 = false;
    this.tip13 = false;
    this.tip14 = false;
    this.tip15 = false;
  }
  tip_2(): void {
    this.tip1 = false;
    this.tip2 = true;
    this.tip3 = false;
    this.tip4 = false;
    this.tip5 = false;
    this.tip6 = false;
    this.tip7 = false;
    this.tip8 = false;
    this.tip9 = false;
    this.tip10 = false;
    this.tip11 = false;
    this.tip12 = false;
    this.tip13 = false;
    this.tip14 = false;
    this.tip15 = false;
  }
  tip_3(): void {
    this.tip1 = false;
    this.tip2 = false;
    this.tip3 = true;
    this.tip4 = false;
    this.tip5 = false;
    this.tip6 = false;
    this.tip7 = false;
    this.tip8 = false;
    this.tip9 = false;
    this.tip10 = false;
    this.tip11 = false;
    this.tip12 = false;
    this.tip13 = false;
    this.tip14 = false;
    this.tip15 = false;
  }
  tip_4(): void {
    this.tip1 = false;
    this.tip2 = false;
    this.tip3 = false;
    this.tip4 = true;
    this.tip5 = false;
    this.tip6 = false;
    this.tip7 = false;
    this.tip8 = false;
    this.tip9 = false;
    this.tip10 = false;
    this.tip11 = false;
    this.tip12 = false;
    this.tip13 = false;
    this.tip14 = false;
    this.tip15 = false;
  }
  tip_5(): void {
    this.tip1 = false;
    this.tip2 = false;
    this.tip3 = false;
    this.tip4 = false;
    this.tip5 = true;
    this.tip6 = false;
    this.tip7 = false;
    this.tip8 = false;
    this.tip9 = false;
    this.tip10 = false;
    this.tip11 = false;
    this.tip12 = false;
    this.tip13 = false;
    this.tip14 = false;
    this.tip15 = false;
  }
  tip_6(): void {
    this.tip1 = false;
    this.tip2 = false;
    this.tip3 = false;
    this.tip4 = false;
    this.tip5 = false;
    this.tip6 = true;
    this.tip7 = false;
    this.tip8 = false;
    this.tip9 = false;
    this.tip10 = false;
    this.tip11 = false;
    this.tip12 = false;
    this.tip13 = false;
    this.tip14 = false;
    this.tip15 = false;
  }
  tip_7(): void {
    this.tip1 = false;
    this.tip2 = false;
    this.tip3 = false;
    this.tip4 = false;
    this.tip5 = false;
    this.tip6 = false;
    this.tip7 = true;
    this.tip8 = false;
    this.tip9 = false;
    this.tip10 = false;
    this.tip11 = false;
    this.tip12 = false;
    this.tip13 = false;
    this.tip14 = false;
    this.tip15 = false;
  }
  tip_8(): void {
    this.tip1 = false;
    this.tip2 = false;
    this.tip3 = false;
    this.tip4 = false;
    this.tip5 = false;
    this.tip6 = false;
    this.tip7 = false;
    this.tip8 = true;
    this.tip9 = false;
    this.tip10 = false;
    this.tip11 = false;
    this.tip12 = false;
    this.tip13 = false;
    this.tip14 = false;
    this.tip15 = false;
  }
  tip_9(): void {
    this.tip1 = false;
    this.tip2 = false;
    this.tip3 = false;
    this.tip4 = false;
    this.tip5 = false;
    this.tip6 = false;
    this.tip7 = false;
    this.tip8 = false;
    this.tip9 = true;
    this.tip10 = false;
    this.tip11 = false;
    this.tip12 = false;
    this.tip13 = false;
    this.tip14 = false;
    this.tip15 = false;
  }
  tip_10(): void {
    this.tip1 = false;
    this.tip2 = false;
    this.tip3 = false;
    this.tip4 = false;
    this.tip5 = false;
    this.tip6 = false;
    this.tip7 = false;
    this.tip8 = false;
    this.tip9 = false;
    this.tip10 = true;
    this.tip11 = false;
    this.tip12 = false;
    this.tip13 = false;
    this.tip14 = false;
    this.tip15 = false;
  }
  tip_11(): void {
    this.tip1 = false;
    this.tip2 = false;
    this.tip3 = false;
    this.tip4 = false;
    this.tip5 = false;
    this.tip6 = false;
    this.tip7 = false;
    this.tip8 = false;
    this.tip9 = false;
    this.tip10 = false;
    this.tip11 = true;
    this.tip12 = false;
    this.tip13 = false;
    this.tip14 = false;
    this.tip15 = false;
  }
  tip_12(): void {
    this.tip1 = false;
    this.tip2 = false;
    this.tip3 = false;
    this.tip4 = false;
    this.tip5 = false;
    this.tip6 = false;
    this.tip7 = false;
    this.tip8 = false;
    this.tip9 = false;
    this.tip10 = false;
    this.tip11 = false;
    this.tip12 = true;
    this.tip13 = false;
    this.tip14 = false;
    this.tip15 = false;
  }
  tip_13(): void {
    this.tip1 = false;
    this.tip2 = false;
    this.tip3 = false;
    this.tip4 = false;
    this.tip5 = false;
    this.tip6 = false;
    this.tip7 = false;
    this.tip8 = false;
    this.tip9 = false;
    this.tip10 = false;
    this.tip11 = false;
    this.tip12 = false;
    this.tip13 = true;
    this.tip14 = false;
    this.tip15 = false;
  }
  tip_14(): void {
    this.tip1 = false;
    this.tip2 = false;
    this.tip3 = false;
    this.tip4 = false;
    this.tip5 = false;
    this.tip6 = false;
    this.tip7 = false;
    this.tip8 = false;
    this.tip9 = false;
    this.tip10 = false;
    this.tip11 = false;
    this.tip12 = false;
    this.tip13 = false;
    this.tip14 = true;
    this.tip15 = false;
  }
  tip_15(): void {
    this.tip1 = false;
    this.tip2 = false;
    this.tip3 = false;
    this.tip4 = false;
    this.tip5 = false;
    this.tip6 = false;
    this.tip7 = false;
    this.tip8 = false;
    this.tip9 = false;
    this.tip10 = false;
    this.tip11 = false;
    this.tip12 = false;
    this.tip13 = false;
    this.tip14 = false;
    this.tip15 = true;
  }

}
