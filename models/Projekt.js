var mongoose = require('mongoose');

var ProjektSchema = new mongoose.Schema({
  naziv: String,
  clan: String,
  clan1: String,
  clan2: String,
  clan3: String,
  clan4: String,
  clan5: String,
  clan6: String,
  clan7: String,
  clan8: String,
  clan9: String,
  tip_ocena: String,
  zgornja_meja_dolga: String,
  datum: { type: Date, default: Date.now },
  error: String,
});

module.exports = mongoose.model('Projekt', ProjektSchema);
